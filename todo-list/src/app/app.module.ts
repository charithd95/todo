import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { TodolistComponent } from './todolist/todolist.component';
import { ItemComponent } from './item/item.component';
import {TodoListService} from './todolist.service';

@NgModule({
  declarations: [
    AppComponent,
    TodolistComponent,
    ItemComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [TodoListService],
  bootstrap: [AppComponent]
})
export class AppModule { }
