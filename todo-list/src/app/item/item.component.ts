import { Component, OnInit,Input, Output } from '@angular/core';
import { TodoListService } from '../todolist.service';
import { EventEmitter } from 'events';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {
  
  @Input() public parentData;

  

  constructor(private service: TodoListService) { }

  ngOnInit() {
  }
  completeTodo(id:number){
    this.service.completeTodo(id);
  }
  deleteTodo(id:number){
    this.service.deleteTodo(id);
    console.log(id);

  }

}
