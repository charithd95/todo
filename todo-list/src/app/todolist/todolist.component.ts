import { Component, OnInit } from '@angular/core'; 
import { TodoListService } from '../todolist.service';
import { Todo } from '../todo';
@Component({
  selector: 'app-todolist',
  templateUrl: './todolist.component.html',
  styleUrls: ['./todolist.component.css']
})
export class TodolistComponent implements OnInit {
  public todos:Todo[];
  constructor(public service: TodoListService) {
   }
   
  ngOnInit() {
    this.todos = this.service.getTodo()
  }
  errorMsg="";
  
  saveTodo(todoInput:string){
    if (this.validateInput(todoInput)){
      this.todos=this.service.insertTodo(todoInput);
    }
    else{
      this.errorMsg="Please Enter text in Todo";
      
    }
  }
  validateInput(todoInput){
    if (todoInput){
      return true;
    }
    else return false;
  }
  

}
