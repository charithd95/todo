import { Injectable } from "@angular/core";
import { Todo } from "./todo";

@Injectable()
export class TodoListService{
    previousId= 0;
    public todolist:Todo[]= [];
    getTodo():Todo[]{
        return this.todolist;
    }
    insertTodo(todo:string):Todo[]{
        let newtodo:Todo= new Todo(todo);
        let id = this.previousId++;
        newtodo.setId(id);
        this.todolist.push(newtodo);
        return this.todolist;
    }
    deleteTodo(id:number){
      this.todolist=this.todolist.filter(t=>t.id!==id);
      console.log(this.todolist);
    }
    completeTodo(completeId:number){
        this.todolist.forEach(element => {
            if(element.id===completeId){
                element.isComplete=true;
            }
            
        });
    }
}